let form = document.getElementById('addForm');

let itemList = document.querySelector('#items');

let filter = document.getElementById('filter');


// Add item 

let addItem = (e) => {

    e.preventDefault();

    //Get input value
    let newItem = document.getElementById('item').value;
    

    //create new li element
    let li = document.createElement('li');

    //Add class
    li.className = 'list-group-item';

    //Add text node with input value
    li.appendChild(document.createTextNode(newItem));

    // Create delete button element
    let deleteBtn = document.createElement('button');

    // Add classes to del button
    deleteBtn.className = 'btn btn-danger btn-sm float-right delete';

    // Append text node
    deleteBtn.appendChild(document.createTextNode('X'));


    // Append button to li
    li.appendChild(deleteBtn);
    
    //Append li to list
    itemList.appendChild(li);
}

// remove item function

let removeItem = (e) => {
    if(e.target.classList.contains('delete')) {
       if(confirm('Are you sure?')) {
           let li = e.target.parentElement;
           itemList.removeChild(li);
       }
    }
}


// Filter items
let filterItems = (e) => {


    //convert text to lowercase
    let text = e.target.value.toLowerCase();
    let items = itemList.getElementsByTagName('li');
    // console.log(items);
    Array.from(items).forEach((item) => {
        let itemName = item.firstChild.textContent;
        // console.log(itemName);
        if(itemName.toLowerCase().indexOf(text) !== -1){
            item.style.display = 'block';
        } else {
            item.style.display = 'none';
        }

    });
}

// Form submit event

form.addEventListener('submit', addItem);

// Delete event

itemList.addEventListener('click', removeItem);


// Filter event

filter.addEventListener('keyup', filterItems);